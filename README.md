# Simple Arithmetic Calculator
Here we create an API that takes an
arithmetic expression as input and return it's
corresponding answer.    

Here the end point is `/calculate` and the method is `GET`.     

Example
```json
// Calling API
{
    "expression": "( 10 + ( 5 / 4 * ( 2 - 4 * 2 ) ) ) / ( 4 + 2 * 7 ) * ( 8 / 2 )"
}
```

```json
// API response
{
    "solution": 0.5555555555555556,
    "status": 200
}
```

Here `status` tells us if `expression` is **valid** or **invalid**.    
`200` means a **valid** `expression` and
`500` means an **invalid** `expression`     

For Example
```json
// Calling API
// Here parentheses are Unbalance
{
    "expression": "2 + (5 * 3 / (2 + 3)"
}
```

```json
// API response
{
    "solution": "Unbalanced parentheses",
    "status": 500
}
```

## Approach
The core Idea is to handle the parentheses,
and for that here we use `stack`.   
First we find all the parentheses in there
recursive order, and then
We recursively solve for all the parentheses.    

## Time and Space Complexity
`n`: represents the length of the `expression`    
Time Complexity: `O(n)`    
Space Complexity: `O(n)`


## What If I have more time
This API is created in 2.5 - 3 hours,
If I have more time I would like to also include
mathematical functions like `sin`, `cos`, `exp`, `log`, `sinh`, etc.     
