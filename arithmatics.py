def __solve__(expression):
    exponents = []
    division = []
    multiplications = []
    additions = []
    subtractions = []
    
    for i, e in enumerate(expression):
        if e == "^":
            exponents.append(i)

    for counter, idx in enumerate(exponents):
        idx = idx - counter * 2
        expression[idx - 1 : idx + 2] = [expression[idx - 1] ** expression[idx + 1]]

    for i, e in enumerate(expression):
        if e == "/":
            division.append(i)
    
    for counter, idx in enumerate(division):
        idx = idx - counter * 2
        expression[idx - 1 : idx + 2] = [expression[idx - 1] / expression[idx + 1]]
    
    for i, e in enumerate(expression):
        if e == "*":
            multiplications.append(i)

    for counter, idx in enumerate(multiplications):
        idx = idx - counter * 2
        expression[idx - 1 : idx + 2] = [expression[idx - 1] * expression[idx + 1]]

    for i, e in enumerate(expression):
        if e == "+":
            additions.append(i)

    for counter, idx in enumerate(additions):
        idx = idx - counter * 2
        expression[idx - 1 : idx + 2] = [expression[idx - 1] + expression[idx + 1]]

    for i, e in enumerate(expression):
        if e == "-":
            subtractions.append(i)

    for counter, idx in enumerate(subtractions):
        idx = idx - counter * 2
        expression[idx - 1 : idx + 2] = [expression[idx - 1] - expression[idx + 1]]
    return expression

def correct_the_order(order, index, counter):
    for i, (start, end) in enumerate(order):
        if start > index:
            start -= counter
        if end > index:
            end -= counter
        order[i] = (start, end)


# It took O(n) time and space complexity
def solve(expression: str):

    # These replaces took O(n) time complexity
    expression = expression.replace(" ", "")
    expression = expression.replace("[", "(")
    expression = expression.replace("{", "(")
    expression = expression.replace("]", ")")
    expression = expression.replace("}", ")")
    expression = "("  + expression + ")"
    
    # print(eval(expression.replace("^", "**")))
    
    # This for loop took O(n) time and space complexity
    new_expression = []
    temp = ""
    for c in expression:
        if c.isnumeric() or c == ".":
            temp += c
        else:
            if temp != "": new_expression.append(float(temp))
            new_expression.append(c)
            temp = ""
    expression = new_expression
    
    # This for loop took O(n) time and space complexity
    stack = []
    order_of_solution = []
    for i, e in enumerate(expression):
        if e != "(" and e != ")":
            continue
        if e == "(":
            stack.append(i)
        else:
            if len(stack) == 0 or expression[stack[-1]] != "(":
                raise ValueError("Unbalanced parentheses")
            else:
                prev_idx = stack.pop()
                order_of_solution.append((prev_idx, i))

    if len(stack) != 0: raise ValueError("Unbalanced parentheses")

    order_of_solution.reverse()
    
    # This while loop took O(n) time and space complexity
    # it's little tricky to figure it out that time complexity is O(n)
    # "order_of_solution" contains indexes of portions of expression
    # we call __solve__ for that specific portions of expression
    # so this whole while loop will take O(n) time complexity
    while len(order_of_solution) != 0:
        start, end = order_of_solution.pop()
        expression[start : end + 1] = __solve__(expression[start + 1 : end])
        correct_the_order(order_of_solution, end, end - start)

    return expression[0]
