from flask import Flask, request
from arithmatics import solve

app = Flask(__name__)

@app.route('/calculate', methods=['GET'])
def analyze():
    req: dict = request.get_json()
    expression = req["expression"]
    try:
        return {"solution": solve(expression), "status": 200}
    except ValueError:
        return {"solution": "Unbalanced parentheses", "status": 500}
    except:
        return {"solution": "Invalid expression", "status": 500}
